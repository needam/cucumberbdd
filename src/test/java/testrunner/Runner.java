package testrunner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src\\test\\resources\\functionalTests"},
        glue = {"stepdefinitions","MyHooks"},
        tags = "@smoke or @regression",
        plugin = {"pretty", "json:target/MyReports/report.json",
                    "junit:target/MyReports/report.xml"}

)
public class Runner {
}
