package MyHooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;

public class AppHooks {

    @Before
   public void setUp(){
        System.out.println("launch the application");
    }

    @After
    public void tearDown(){
        System.out.println("quit the browser");
    }
}
