package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class UberStepDefs {
    @Given("user select location in the app")
    public void userSelectLocationInTheApp() {
        System.out.println("select location");
    }

    @When("^user select car \"([^\"]*)\" from app$")
    public void userSelectCarFromApp(String carType) {
        System.out.println(carType);
    }

    @Then("driver starts the journey")
    public void driverStartsTheJourney() {
        System.out.println("start journey");
    }

    @And("driver ends the journey")
    public void driverEndsTheJourney() {
        System.out.println("end journey");
    }

    @Then("user pays {int} USD")
    public void userPaysUSD(int price) {
        System.out.println(price);
    }
}
