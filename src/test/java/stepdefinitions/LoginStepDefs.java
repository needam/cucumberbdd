package stepdefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import javax.sound.midi.Soundbank;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class LoginStepDefs {
    @Given("user is on homepage")
    public void userIsOnHomepage() {
        System.out.println("homepage");
    }

    @When("user navigates to login page")
    public void userNavigatesToLoginPage() {
        System.out.println("navigate");
    }


    @Then("user is logged in successfully")
    public void userIsLoggedInSuccessfully() {
        System.out.println("loggedin");
    }

    @And("user enter credentials to login")
    public void userEnterCredentialsToLogin(DataTable userCredentials) {
        List<Map<String,String>> data = userCredentials.asMaps(String.class,String.class);
        System.out.println(data.get(0).get("username"));
        System.out.println(data.get(0).get("password"));
    }

    @And("user enter credentials to login with multiple data")
    public void userEnterCredentialsToLoginWithMultipleData(DataTable credentials) {
        for(Map<Object, Object> data: credentials.asMaps(String.class,String.class)){
            System.out.println(data.get("username"));
            System.out.println(data.get("password"));
        }
    }

    @And("user enter credentials map datatable as lists")
    public void userEnterCredentialsMapDatatableAsLists(DataTable dataTable) {
        List<List<String>> userList = dataTable.asLists(String.class);
        for(List<String> e: userList){
            System.out.println(e);
           String result = e.get(0);
            System.out.println(result);
            Iterator<String> itr = e.iterator();
            while(itr.hasNext()){
                System.out.println(itr.next());
            }
        }
    }
}
