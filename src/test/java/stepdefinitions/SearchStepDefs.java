package stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class SearchStepDefs {
    @Given("I have a search field on amazon page")
    public void iHaveASearchFieldOnAmazonPage() {
        System.out.println("I'm on search page");
    }

    @When("I search for a product with name {string} and price {int}")
    public void iSearchForAProductWithNameAndPrice(String productName, int price) {
        System.out.println("searched a product named "+productName+" with price "+price);
    }

    @Then("product with name {string} should be displayed")
    public void productWithNameShouldBeDisplayed(String productName) {
        System.out.println("product with name"+productName+ " is displayed");
    }

    @Given("user go to Amazon page")
    public void userGoToAmazonPage() {
        System.out.println("go to url");
    }
}
