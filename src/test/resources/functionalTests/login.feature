Feature: Login to gmail
  @smoke
  Scenario: login single data
    Given user is on homepage
    When user navigates to login page
    And user enter credentials to login
      |username|password|
      |testuser01|pass@123|
    Then user is logged in successfully

  @smoke
  Scenario: login multiple data
    Given user is on homepage
    When user navigates to login page
    And user enter credentials to login with multiple data
      |username|password|
      |testuser01|pass@123|
      |testuser02|pass@789|
    Then user is logged in successfully

  @smoke
  Scenario: datatable as lists
    Given user is on homepage
    When user navigates to login page
    And user enter credentials map datatable as lists
      |testuser01|pass@123|xyz@gmail.com|9999|address|
      |testuser02|pass@789|ild@gmail.com|9876|adress |
    Then user is logged in successfully