Feature: book a cab
  @smoke
  Scenario: book cab from uber sedan
    Given user select location in the app
    When user select car "sedan" from app
    Then driver starts the journey
    And driver ends the journey
    Then user pays 1000 USD

  @regression
  Scenario: book cab from uber suv
    Given user select location in the app
    When user select car "sedan" from app
    Then driver starts the journey
    And driver ends the journey
    Then user pays 1000 USD

  @smoke
  Scenario: book cab from uber mini
    Given user select location in the app
    When user select car "sedan" from app
    Then driver starts the journey
    And driver ends the journey
    Then user pays 1000 USD
