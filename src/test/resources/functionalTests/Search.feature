Feature: Amazon search

  Background:
    #repeated steps of each scenario
    Given I have a search field on amazon page

  @smoke
  Scenario: Search a product iphone
    When I search for a product with name "Apple Macbook Pro" and price 1000
    Then product with name "Apple Macbook Pro" should be displayed

  @regression
  Scenario: Search a product android
    When I search for a product with name "android" and price 1000
    Then product with name "Android" should be displayed

  @regression @smoke
  Scenario Outline: Search a product mac
    When I search for a product with name "<productName>" and price 1000
    Then product with name "Android" should be displayed

    Examples:
      | productName |
      | mac         |
      | iphone      |
      | Android     |
